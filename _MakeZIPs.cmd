@Echo off

md D:\Temp\Mod\res_mods\%WotVer%\scripts\client\gui\mods
md D:\Temp\Mod\res_mods\configs\BBMods

copy M:\ModPack\Mods\BBModsSDK.pyc D:\Temp\Mod\res_mods\%WotVer%\scripts\client\gui\mods
copy M:\ModPack\Docs\License.txt D:\Temp\Mod\
copy "M:\ModPack\Docs\*.url" D:\Temp\Mod\

if %1==Auxilium (
	copy "M:\ModPack\Docs\Auxilium ListChange.txt" D:\Temp\Mod\
	copy M:\ModPack\Mods\mod_Auxilium.pyc D:\Temp\Mod\res_mods\%WotVer%\scripts\client\gui\mods
	copy M:\ModPack\Auxilium\Auxilium.json D:\Temp\Mod\res_mods\configs\BBMods
	copy M:\ModPack\Auxilium\Configs\loginapp_wotch.pubkey D:\Temp\Mod\res_mods\%WotVer%
	copy M:\ModPack\Auxilium\Configs\scripts_config.xml D:\Temp\Mod\res_mods\%WotVer%
)
if %1==TimeSpent (
	md D:\Temp\Mod\res_mods\configs\BBMods\Default
	md D:\Temp\Mod\res_mods\configs\BBMods\StatisticResults
	md D:\Temp\Mod\OtherSkins
	copy "M:\ModPack\Docs\TimeSpent ListChange.txt" D:\Temp\Mod\
	copy "M:\ModPack\Docs\MacrosList.txt" D:\Temp\Mod\
	copy "M:\ModPack\Docs\BattleTypes.txt" D:\Temp\Mod\
	copy M:\ModPack\Mods\mod_TimeSpent.pyc D:\Temp\Mod\res_mods\%WotVer%\scripts\client\gui\mods
	copy M:\ModPack\TimeSpent\Default\*.* D:\Temp\Mod\res_mods\configs\BBMods\Default\
	copy M:\ModPack\TimeSpent\Minimalistic.json D:\Temp\Mod\res_mods\configs\BBMods\TimeSpent.json
	xcopy M:\ModPack\TimeSpent\* D:\Temp\Mod\OtherSkins /e
)
if %1==AntiToxicity (
	copy "M:\ModPack\Docs\AntiToxicity ListChange.txt" D:\Temp\Mod\
	copy M:\ModPack\Mods\mod_antitoxicity.pyc D:\Temp\Mod\res_mods\%WotVer%\scripts\client\gui\mods
	copy M:\ModPack\AntiToxicity\AntiToxicity.json D:\Temp\Mod\res_mods\configs\BBMods\
)
if %1==HDMinimap (
	md D:\Temp\Mod\res_mods\%WotVer%\gui\flash
	copy M:\ModPack\Mods\minimapCenter.swf D:\Temp\Mod\res_mods\%WotVer%\gui\flash
	copy "M:\ModPack\Docs\HDMinimap ListChange.txt" D:\Temp\Mod\
	copy M:\ModPack\Mods\mod_HDMinimap.pyc D:\Temp\Mod\res_mods\%WotVer%\scripts\client\gui\mods
	copy M:\ModPack\HDMinimap\HDMinimap.json D:\Temp\Mod\res_mods\configs\BBMods\
	xcopy M:\ModPack\HDMinimap\Maps\* D:\Temp\Mod\res_mods\configs\BBMods\ /e
)
if %1==UnlimitedBlacklist (
	copy "M:\ModPack\Docs\AntiToxicity ListChange.txt" D:\Temp\Mod\
	copy M:\ModPack\Mods\mod_antitoxicity.pyc D:\Temp\Mod\res_mods\%WotVer%\scripts\client\gui\mods
	copy M:\ModPack\AntiToxicity\AntiToxicityBL.json D:\Temp\Mod\res_mods\configs\BBMods\AntiToxicity.json
)
if %1==noWGStat (
	md D:\Temp\Mod\res_mods\configs\BBMods\Auxilium
	copy "M:\ModPack\Docs\Auxilium ListChange.txt" D:\Temp\Mod\
	copy M:\ModPack\Mods\mod_Auxilium.pyc D:\Temp\Mod\res_mods\%WotVer%\scripts\client\gui\mods
	copy M:\ModPack\Auxilium\Configs\noStatsBtn.json D:\Temp\Mod\res_mods\configs\BBMods\Auxilium
)

"%ProgramFiles%\7-Zip\7z.exe" a -tzip -r D:\Temp\%1.zip D:\Temp\Mod\*

rd /s /q D:\Temp\Mod
