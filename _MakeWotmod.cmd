@Echo off

md D:\Temp\Mod\mods\%WotVer%\
md D:\Temp\Mod\res\scripts\client\gui\mods
md D:\Temp\Mod\mods\configs\BBMods

copy M:\ModPack\Mods\BBModsSDK.pyc D:\Temp\Mod\res\scripts\client\gui\mods
copy M:\ModPack\Mods\mod_Auxilium.pyc D:\Temp\Mod\res\scripts\client\gui\mods
copy M:\ModPack\Auxilium\Configs\loginapp_wotch.pubkey D:\Temp\Mod\res
copy M:\ModPack\Auxilium\Configs\scripts_config.xml D:\Temp\Mod\res
"%ProgramFiles%\7-Zip\7z.exe" a -tzip -mx0 D:\Temp\Mod\mods\%WotVer%\ShuraBB.%1.wotmod D:\Temp\Mod\res\
rd /s /q D:\Temp\Mod\res

copy M:\ModPack\Docs\License.txt D:\Temp\Mod\
copy "M:\ModPack\Docs\*.url" D:\Temp\Mod\
copy "M:\ModPack\Docs\Auxilium ListChange.txt" D:\Temp\Mod\
copy M:\ModPack\Auxilium\Configs\MultiClient.json D:\Temp\Mod\mods\configs\BBMods\Auxilium.json

"%ProgramFiles%\7-Zip\7z.exe" a -tzip -r D:\Temp\%1.zip D:\Temp\Mod\*.*
rd /s /q D:\Temp\Mod
