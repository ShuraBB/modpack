﻿#define MyAppName		"Сборник модов от ShuraBB"
#define ReleaseVersion	"032"
#define BildVersion		"01"
#define MyAppURL		"http://bb-t.ru/bbmods/"

#include "Main.iss"
#include "Scripts\FileOperation.iss"
#include "Scripts\Messages.iss"
#include "Scripts\Components_Hangar.iss"
#include "Scripts\Components_Battle.iss"
#include "Scripts\Components_Stat.iss"
#include "Scripts\Components_Other.iss"
;#include "Scripts\Components_Scripts.iss"
#include "Scripts\Files_Auxilium.iss"
#include "Scripts\Files_AntiToxicity.iss"
#include "Scripts\Files_Timespent.iss"
#include "Scripts\Files_Minimap.iss"
#include "Scripts\Files_Other.iss"
#include "Scripts\Run.iss"
#include "Scripts\Description.iss"

#include "Scripts\Code.iss"

[Setup]
AppId={{79B447F3-5874-4281-91F3-5A1C5FE8839F}
SetupIconFile = Images\favicon.ico
WizardSmallImageFile = Images\ShuraBB.bmp
OutputBaseFilename = ModPack_ShuraBB-{#ReleaseVersion}.{#BildVersion}
UninstallDisplayIcon = {userappdata}\Wargaming.net\BBMods\unins000.exe
UninstallFilesDir = {userappdata}\Wargaming.net\BBMods
UninstallDisplayName = {cm:Main} {cm:WoTVersion} {#ReleaseVersion}

[Languages]
Name: ru; MessagesFile: "compiler:Default.isl"; LicenseFile: readme_ru.txt; InfoBeforeFile: ListChange.txt; InfoAfterFile: Docs\Donates.txt
Name: en; MessagesFile: "compiler:Languages\English.isl"; LicenseFile: readme_en.txt; InfoBeforeFile: ListChange_en.txt; InfoAfterFile: Docs\Donates.txt

[Messages]
ru.WelcomeLabel1=Сборник модов от ShuraBB
en.WelcomeLabel1=Mod installer by ShuraBB
ru.SelectComponentsDesc=Выберите компоненты, которые вы хотите установить; снимите флажки с компонентов, устанавливать которые не требуется. Нажмите «Далее», когда вы будете готовы продолжить.
en.SelectComponentsDesc=Select the components you want to install; clear the components you do not want to install. Click Next when you are ready to continue.

[CustomMessages]
ru.Main =Сборник модов от ShuraBB
en.Main =ModPack by ShuraBB

