@Echo off

for %%F in (ShuraBB.MinimalisticSight) do (
	md D:\Temp\Mod\mods\%WotVer%\
	copy M:\ModPack\Other\%%F.wotmod D:\Temp\Mod\mods\%WotVer%\
	copy M:\ModPack\Docs\License.txt D:\Temp\Mod\
	copy "M:\ModPack\Docs\*.url" D:\Temp\Mod\

	"%ProgramFiles%\7-Zip\7z.exe" a -tzip -r D:\Temp\%%F.zip D:\Temp\Mod\*.*
	rd /s /q D:\Temp\Mod
	)
