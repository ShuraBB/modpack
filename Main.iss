﻿#define MyAppPublisher		"ShuraBB"
#define ResModsDest			"{app}\res_mods\{cm:WoTVersion}\scripts\client\gui\mods"
#define ModsDest			"{app}\mods\{cm:WoTVersion}"
#define ResModsConf			"{app}\res_mods\configs\BBMods"
#define ModsConf			"{app}\mods\configs\BBMods"
#define AudioDest			"{app}\res\audioww"
#define GuiDest				"{app}\res_mods\{cm:WoTVersion}\gui"


[Setup]
AppName = {cm:Main}
AppVersion = {cm:WoTVersion}.{#ReleaseVersion}
AppVerName = {cm:Main} #{#ReleaseVersion}
AppPublisher = {#MyAppPublisher}
AppPublisherURL = {#MyAppURL}
AppSupportURL = {#MyAppURL}
AppUpdatesURL = {#MyAppURL}
VersionInfoDescription = {#MyAppName}
VersionInfoProductName = {#MyAppName}
VersionInfoCopyright = {#MyAppPublisher} {#MyAppURL}
VersionInfoVersion = 1.5.{#ReleaseVersion}.{#BildVersion}
DefaultDirName = {code:GetAppFolder}
DisableProgramGroupPage = yes
DirExistsWarning = no
AppendDefaultDirName = no
AllowNoIcons = yes
ComponentsListTVStyle = yes
WizardImageFile = Images\lft.bmp
OutputDir = D:\Temp
InternalCompressLevel=ultra64
Compression=lzma2/ultra64


[Types]
Name: None; Description: {cm:None};
Name: Default; Description: {cm:DefaultTypes};
Name: Custom; Description: {cm:CustomTypes}; Flags: iscustom
;Name: Scripts; Description: {cm:Scripts};

[Messages]
BeveledLabel= (c) ShuraBB

[CustomMessages]
WoTVersion=1.6.0.2
ru.None =Пустая конфигурация
en.None =Empty configuration
ru.DefaultTypes =Рекомендуемая конфигурация
en.DefaultTypes =Recommended configuration
ru.CustomTypes =Пользовательская конфигурация
en.CustomTypes =Custom configuration
