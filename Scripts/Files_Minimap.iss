[InstallDelete]
Type: files; Name: "{#ResModsDest}\mod_HDMinimap.pyc"; Components: not Battle\MiniMap;
Type: files; Name: "{#ResModsConf}\HDMinimap.json"; Components: not Battle\MiniMap;
Type: filesandordirs; Name: "{#ResModsConf}\HDMinimap"; Components: not Battle\MiniMap;
Type: filesandordirs; Name: "{#ResModsConf}\HDMaps"; Components: not (Battle\MiniMap\MapMain\MapHD Battle\MiniMap\MapAlt\MapHD Battle\MiniMap\MapIcons\MapHD);
Type: filesandordirs; Name: "{#ResModsConf}\TacticalMaps"; Components: not (Battle\MiniMap\MapMain\MapTact Battle\MiniMap\MapAlt\MapTact Battle\MiniMap\MapIcons\MapTact);
Type: filesandordirs; Name: "{#ResModsConf}\HeatMaps"; Components: not (Battle\MiniMap\MapMain\MapHM Battle\MiniMap\MapAlt\MapHM);

[Files]
Source: "Mods\mod_HDMinimap.pyc"; DestDir: "{#ResModsDest}"; Components: Battle\MiniMap;
Source: "HDMinimap\Configs\HDMinimap.json"; DestDir: "{#ResModsConf}"; Components: Battle\MiniMap;
Source: "Mods\minimapCenter.swf"; DestDir: "{#GuiDest}\flash"; Components: Battle\MiniMap\MapCentr;
Source: "HDMinimap\Configs\MapCentr.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapCentr;
Source: "HDMinimap\Configs\MapDView.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapDView;
Source: "HDMinimap\Configs\MapHAngle.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapHAngle;
Source: "HDMinimap\Configs\MapLabel.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapLabel;
Source: "HDMinimap\Configs\MainMapHD.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapMain\MapHD;
Source: "HDMinimap\Configs\MainTactical.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapMain\MapTact;
Source: "HDMinimap\Configs\MainHeat.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapMain\MapHM;
Source: "HDMinimap\Configs\AltMapHD.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapAlt\MapHD;
Source: "HDMinimap\Configs\AltTactical.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapAlt\MapTact;
Source: "HDMinimap\Configs\AltHeat.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapAlt\MapHM;
Source: "HDMinimap\Configs\IconsMapHD.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapIcons\MapHD;
Source: "HDMinimap\Configs\IconsTactical.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapIcons\MapTact;
Source: "HDMinimap\Maps\HDMaps\*"; DestDir: "{#ResModsConf}\HDMaps"; Components: Battle\MiniMap\MapMain\MapHD Battle\MiniMap\MapAlt\MapHD Battle\MiniMap\MapIcons\MapHD;
Source: "HDMinimap\Maps\TacticalMaps\*"; DestDir: "{#ResModsConf}\TacticalMaps"; Components: Battle\MiniMap\MapMain\MapTact Battle\MiniMap\MapAlt\MapTact Battle\MiniMap\MapIcons\MapTact;
Source: "HDMinimap\Maps\HeatMaps\*"; DestDir: "{#ResModsConf}\HeatMaps"; Components: Battle\MiniMap\MapMain\MapHM Battle\MiniMap\MapAlt\MapHM;
Source: "HDMinimap\Configs\MapCircle1.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapCircle\MapCircle1;
Source: "HDMinimap\Configs\MapCircle2.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\MapCircle\MapCircle2;
Source: "HDMinimap\Configs\ShowDestroyed.json"; DestDir: "{#ResModsConf}\HDMinimap"; Components: Battle\MiniMap\Destroyed;