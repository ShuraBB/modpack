[Components]
; *** �������� ������� ***

; ������� ����������
Name: Hangar; Description: {cm:HangarMods}; Flags: disablenouninstallwarning Collapsed
Name: Hangar\Multi; Description: {cm:Multi}; Types: Default; Flags: disablenouninstallwarning
Name: Hangar\Server; Description: {cm:Server}; Flags: disablenouninstallwarning Collapsed
Name: Hangar\Server\P; Description: {cm:PServer}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Hangar\Server\L; Description: {cm:LServer}; Flags: disablenouninstallwarning exclusive
Name: Hangar\AReturn; Description: {cm:AutoReturn}; Flags: disablenouninstallwarning Collapsed
Name: Hangar\AReturn\Equip; Description: {cm:AEquip}; Types: Default; Flags: disablenouninstallwarning
Name: Hangar\AReturn\Outfit; Description: {cm:AOutfit}; Types: Default; Flags: disablenouninstallwarning
Name: Hangar\AReturn\AutoCamo; Description: {cm:AutoCamo}; Types: Default; Flags: disablenouninstallwarning
Name: Hangar\AReturn\AutoCrew; Description: {cm:AutoCrew}; Types: Default; Flags: disablenouninstallwarning
Name: Hangar\AReturn\NoCamo; Description: {cm:NoCamo}; Flags: disablenouninstallwarning Collapsed
Name: Hangar\AReturn\NoCamo\NoEquip; Description: {cm:NoEquip}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Hangar\AReturn\NoCamo\AutoBuy; Description: {cm:AutoBuy}; Flags: disablenouninstallwarning exclusive
Name: Hangar\AReturn\AKeyR; Description: {cm:AKeyR}; Flags: disablenouninstallwarning
Name: Hangar\AReturn\AKeyOff; Description: {cm:AKeyOff}; Flags: disablenouninstallwarning
Name: Hangar\Carousel; Description: {cm:Carousel}; Flags: disablenouninstallwarning Collapsed
Name: Hangar\Carousel\0; Description: {cm:Car0}; Flags: disablenouninstallwarning exclusive
Name: Hangar\Carousel\1; Description: {cm:Car1}; Flags: disablenouninstallwarning exclusive
Name: Hangar\Carousel\2; Description: {cm:Car2}; Flags: disablenouninstallwarning exclusive
Name: Hangar\Carousel\3; Description: {cm:Car3}; Flags: disablenouninstallwarning exclusive
Name: Hangar\Carousel\4; Description: {cm:Car4}; Flags: disablenouninstallwarning exclusive
Name: Hangar\Carousel\5; Description: {cm:NoCrewMsg}; Types: Default; Flags: disablenouninstallwarning
Name: Hangar\LockGold; Description: {cm:LockGold}; Flags: disablenouninstallwarning
Name: Hangar\LockFreeXp; Description: {cm:LockFreeXp}; Flags: disablenouninstallwarning
Name: Hangar\HeroTank; Description: {cm:HeroTank}; Types: Default; Flags: disablenouninstallwarning

; ������� ��������������
Name: Hangar\fl; Description: {cm:fl}; Flags: disablenouninstallwarning Collapsed
Name: Hangar\fl\500; Description: 500; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Hangar\fl\1000; Description: 1000; Flags: disablenouninstallwarning exclusive
Name: Hangar\bl; Description: {cm:bl}; Flags: disablenouninstallwarning Collapsed
Name: Hangar\bl\2000; Description: 2000; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Hangar\bl\3000; Description: 3000; Flags: disablenouninstallwarning exclusive
Name: Hangar\bl\reset; Description: {cm:ResetKey}; Flags: disablenouninstallwarning
Name: Hangar\CU; Description: {cm:CUFilters}; Flags: disablenouninstallwarning Collapsed
Name: Hangar\CU\PopUp; Description: {cm:PopUpMessages}; Types: Default; Flags: disablenouninstallwarning
Name: Hangar\CU\PopUpD; Description: {cm:DisablePopUp}; Flags: disablenouninstallwarning dontinheritcheck
Name: Hangar\CU\System; Description: {cm:SystemMessages}; Types: Default; Flags: disablenouninstallwarning
Name: Hangar\CU\SystemD; Description: {cm:DisableSystem}; Flags: disablenouninstallwarning dontinheritcheck

