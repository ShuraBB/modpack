[Components]
; Сессионная статистика
Name: Stat; Description: {cm:Statistics}; Flags: disablenouninstallwarning Collapsed
Name: Stat\D; Description: {cm:Skins}; Flags: disablenouninstallwarning exclusive Collapsed
Name: Stat\D\01; Description: {cm:Minimalistic}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Stat\D\04; Description: {cm:Simple}; Flags: disablenouninstallwarning exclusive
Name: Stat\D\05; Description: {cm:Zero}; Flags: disablenouninstallwarning exclusive
Name: Stat\D\03; Description: {cm:Lite}; Flags: disablenouninstallwarning exclusive
Name: Stat\D\02; Description: {cm:Default}; Flags: disablenouninstallwarning exclusive
Name: Stat\02; Description: {cm:Armagomen}; Flags: disablenouninstallwarning exclusive
Name: Stat\03; Description: {cm:Kapany3uk}; Flags: disablenouninstallwarning exclusive Collapsed
Name: Stat\03\1; Description: {cm:Kapany3uk1}; Flags: disablenouninstallwarning exclusive
Name: Stat\03\2; Description: {cm:Kapany3uk2}; Flags: disablenouninstallwarning exclusive
Name: Stat\03\3; Description: {cm:Kapany3uk3}; Flags: disablenouninstallwarning exclusive
Name: Stat\03\4; Description: {cm:Kapany3uk4}; Flags: disablenouninstallwarning exclusive
Name: Stat\04; Description: {cm:M1kE_NRG}; Flags: disablenouninstallwarning exclusive
Name: Stat\05; Description: {cm:MODER}; Flags: disablenouninstallwarning exclusive
Name: Stat\06; Description: {cm:NooBooL}; Flags: disablenouninstallwarning exclusive
Name: Stat\07; Description: {cm:professors}; Flags: disablenouninstallwarning exclusive
Name: Stat\08; Description: {cm:Xotabych}; Flags: disablenouninstallwarning exclusive Collapsed
Name: Stat\08\1; Description: {cm:Xotabych1}; Flags: disablenouninstallwarning exclusive
Name: Stat\08\2; Description: {cm:Xotabych2}; Flags: disablenouninstallwarning exclusive
Name: Stat\09; Description: {cm:EZorg}; Flags: disablenouninstallwarning exclusive
Name: Stat\10; Description: {cm:mcg00}; Flags: disablenouninstallwarning exclusive
Name: Stat\12; Description: {cm:bes_1}; Flags: disablenouninstallwarning exclusive
Name: Stat\13; Description: {cm:CKirill}; Flags: disablenouninstallwarning exclusive
Name: Stat\14; Description: {cm:YKMain}; Flags: disablenouninstallwarning exclusive Collapsed
Name: Stat\14\v; Description: {cm:YKVanilla}; Flags: disablenouninstallwarning exclusive
Name: Stat\14\1; Description: {cm:YKV1}; Flags: disablenouninstallwarning exclusive
Name: Stat\14\2; Description: {cm:YKV2}; Flags: disablenouninstallwarning exclusive
Name: Stat\14\3; Description: {cm:YKV3}; Flags: disablenouninstallwarning exclusive
Name: Stat\15; Description: {cm:wlad1164}; Flags: disablenouninstallwarning exclusive
Name: Stat\16; Description: {cm:turokk}; Flags: disablenouninstallwarning exclusive
Name: Stat\18; Description: {cm:Magistr}; Flags: disablenouninstallwarning exclusive Collapsed
Name: Stat\18\s; Description: {cm:MagistrS}; Flags: disablenouninstallwarning
Name: Stat\18\s\1; Description: {cm:Magistr1}; Flags: disablenouninstallwarning exclusive
Name: Stat\18\s\2; Description: {cm:Magistr2}; Flags: disablenouninstallwarning exclusive
Name: Stat\18\r; Description: {cm:MagistrR}; Flags: disablenouninstallwarning
Name: Stat\18\r\1; Description: {cm:Magistr3}; Flags: disablenouninstallwarning exclusive
Name: Stat\18\r\2; Description: {cm:Magistr4}; Flags: disablenouninstallwarning exclusive
Name: Stat\19; Description: {cm:vlad_cssr}; Flags: disablenouninstallwarning exclusive
Name: Stat\17; Description: {cm:ShowIn}; Flags: disablenouninstallwarning Collapsed
Name: Stat\17\1; Description: {cm:ShowIn1}; Flags: disablenouninstallwarning exclusive
Name: Stat\17\2; Description: {cm:ShowIn2}; Flags: disablenouninstallwarning exclusive
Name: Stat\17\3; Description: {cm:ShowIn3}; Flags: disablenouninstallwarning exclusive fixed
;Name: Stat\18; Description: {cm:CacheFile}; Flags: disablenouninstallwarning Collapsed
;Name: Stat\18\1; Description: {cm:CacheFile1}; Flags: disablenouninstallwarning exclusive
;Name: Stat\18\2; Description: {cm:CacheFile2}; Flags: disablenouninstallwarning exclusive
