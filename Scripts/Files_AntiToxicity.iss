[InstallDelete]
Type: files; Name: "{#ResModsDest}\mod_antitoxicity.pyc"; Components: not (Hangar\fl Hangar\bl Hangar\CU);
Type: files; Name: "{#ResModsConf}\AntiToxicity.json"; Components: not (Hangar\fl Hangar\bl Hangar\CU);
Type: filesandordirs; Name: "{#ResModsConf}\AntiToxicity";

[Files]
Source: "Mods\mod_antitoxicity.pyc"; DestDir: "{#ResModsDest}"; Components: Hangar\fl Hangar\bl Hangar\CU;
Source: "AntiToxicity\Configs\AntiToxicity.json"; DestDir: "{#ResModsConf}"; Components: Hangar\fl Hangar\bl Hangar\CU
Source: "AntiToxicity\Configs\fl500.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Hangar\fl\500;
Source: "AntiToxicity\Configs\fl1000.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Hangar\fl\1000;
Source: "AntiToxicity\Configs\bl2000.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Hangar\bl\2000;
Source: "AntiToxicity\Configs\bl3000.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Hangar\bl\3000;
Source: "AntiToxicity\Configs\blreset.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Hangar\bl\reset;
Source: "AntiToxicity\Configs\PopUp.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Hangar\CU\PopUp; Languages: ru;
Source: "AntiToxicity\Configs\PopUpEn.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Hangar\CU\PopUp; Languages: en;
Source: "AntiToxicity\Configs\PopUpD.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Hangar\CU\PopUpD;
Source: "AntiToxicity\Configs\System.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Hangar\CU\System; Languages: ru;
Source: "AntiToxicity\Configs\SystemEn.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Hangar\CU\System; Languages: en;
Source: "AntiToxicity\Configs\SystemD.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Hangar\CU\SystemD;

Source: "AntiToxicity\Configs\MapFilter.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\MiniMap\MapFilter;
Source: "AntiToxicity\Configs\ClickDead.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\MiniMap\ClickDead;
Source: "AntiToxicity\Configs\DelMessages.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\MiniMap\DelMessages;
Source: "AntiToxicity\Configs\Duration1.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\MiniMap\Duration\s1;
Source: "AntiToxicity\Configs\Duration2.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\MiniMap\Duration\s2;
Source: "AntiToxicity\Configs\Duration3.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\MiniMap\Duration\s3;
Source: "AntiToxicity\Configs\Duration4.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\MiniMap\Duration\s4;
Source: "AntiToxicity\Configs\ChatFilter.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\ChatFilter;
Source: "AntiToxicity\Configs\ChatMessages.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\ChatFilter; Languages: ru;
Source: "AntiToxicity\Configs\ChatMessagesEn.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\ChatFilter; Languages: en;
Source: "AntiToxicity\Configs\NotFilterYou.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\NotFilterYou;
Source: "AntiToxicity\Configs\ReplayChat.json"; DestDir: "{#ResModsConf}\AntiToxicity"; Components: Battle\ReplayChat;

