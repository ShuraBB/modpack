[InstallDelete]
Type: files; Name: "{#ResModsDest}\mod_TimeSpent.pyc"; Components: not Stat;
Type: files; Name: "{#ResModsConf}\TimeSpent.json"; Components: not Stat;
Type: filesandordirs; Name: "{#ResModsConf}\Default"; Components: not Stat\D;
Type: filesandordirs; Name: "{#ResModsConf}\Armagomen"; Components: not Stat\02;
Type: filesandordirs; Name: "{#ResModsConf}\Kapany3uk"; Components: not Stat\03;
Type: filesandordirs; Name: "{#ResModsConf}\M1kE_NRG"; Components: not Stat\04;
Type: filesandordirs; Name: "{#ResModsConf}\MODER"; Components: not Stat\05;
Type: filesandordirs; Name: "{#ResModsConf}\NooBooL"; Components: not Stat\06;
Type: filesandordirs; Name: "{#ResModsConf}\professors"; Components: not Stat\07;
Type: filesandordirs; Name: "{#ResModsConf}\Xotabych"; Components: not Stat\08;
Type: filesandordirs; Name: "{#ResModsConf}\EZorg"; Components: not Stat\09;
Type: filesandordirs; Name: "{#ResModsConf}\mcg00"; Components: not Stat\10;
Type: filesandordirs; Name: "{#ResModsConf}\_bes_1"; Components: not Stat\12;
Type: filesandordirs; Name: "{#ResModsConf}\Chepizhko_Kirill"; Components: not Stat\13;
Type: filesandordirs; Name: "{#ResModsConf}\YasenKrasen"; Components: not Stat\14;
Type: filesandordirs; Name: "{#ResModsConf}\wlad1164"; Components: not Stat\15;
Type: filesandordirs; Name: "{#ResModsConf}\2rokk"; Components: not Stat\16;
Type: filesandordirs; Name: "{#ResModsConf}\Magistr"; Components: not Stat\18;
Type: filesandordirs; Name: "{#ResModsConf}\vlad_cs_sr"; Components: not Stat\19;

[Dirs]
Name: "{#ResModsConf}\StatisticResults"; Components: Stat\D Stat\02 Stat\08 Stat\07 Stat\14;

[Files]
Source: "Mods\mod_TimeSpent.pyc"; DestDir: "{#ResModsDest}"; Components: Stat;

Source: "Fonts\PartnerUltraCondensed.ttf"; DestDir: "{fonts}"; FontInstall: "PartnerUltraCondensed Ultra Condensed"; Flags: onlyifdoesntexist uninsneveruninstall; Components: Stat\03 Stat\15;
Source: "Fonts\Wingdings 3.ttf"; DestDir: "{fonts}"; FontInstall: "Wingdings 3"; Flags: onlyifdoesntexist uninsneveruninstall; Components: Stat\15;
Source: "Fonts\EuroStyle.ttf"; DestDir: "{fonts}"; FontInstall: "EuroStyle Normal"; Flags: onlyifdoesntexist uninsneveruninstall; Components: Stat\04;
Source: "Fonts\ENIGMAU.ttf"; DestDir: "{fonts}"; FontInstall: "Enigmatic Unicode"; Flags: onlyifdoesntexist uninsneveruninstall; Components: Stat\08;
Source: "Fonts\ProtoSans25.otf"; DestDir: "{fonts}"; FontInstall: "Proto Sans 25"; Flags: onlyifdoesntexist uninsneveruninstall; Components: Stat\16;

Source: "TimeSpent\Default\*"; DestDir: "{#ResModsConf}\Default\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\D;
Source: "TimeSpent\Minimalistic.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\D\01;
Source: "TimeSpent\Simple.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\D\04;
Source: "TimeSpent\Zero.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\D\05;
Source: "TimeSpent\Lite.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\D\03;
Source: "TimeSpent\Default.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\D\02;

Source: "TimeSpent\Armagomen\*"; DestDir: "{#ResModsConf}\Armagomen\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\02;
Source: "TimeSpent\Armagomen.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\02;

Source: "TimeSpent\Kapany3uk\*"; DestDir: "{#ResModsConf}\Kapany3uk\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\03;
Source: "TimeSpent\Kapany3uk.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\03\1;
Source: "TimeSpent\Kapany3uk4.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\03\2;
Source: "TimeSpent\Kapany3uk5.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\03\3;
Source: "TimeSpent\Kapany3uk7.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\03\4;

Source: "TimeSpent\M1kE_NRG\*"; DestDir: "{#ResModsConf}\M1kE_NRG\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\04;
Source: "TimeSpent\M1kE_NRG.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\04;

Source: "TimeSpent\MODER\*"; DestDir: "{#ResModsConf}\MODER\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\05;
Source: "TimeSpent\MODER.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\05;

Source: "TimeSpent\NooBooL\NooB.json"; DestDir: "{#ResModsConf}\NooBooL\"; Flags: ignoreversion; Components: Stat\06;
Source: "TimeSpent\NooBooL\images\*"; DestDir: "{#ResModsConf}\NooBooL\images\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\06;
Source: "TimeSpent\NooBooL\gui\*"; DestDir: "{app}\res_mods\{cm:WoTVersion}\gui\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\06;
Source: "TimeSpent\NooBooL.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\06;

Source: "TimeSpent\professors\*"; DestDir: "{#ResModsConf}\professors\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\07;
Source: "TimeSpent\professors.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\07;

Source: "TimeSpent\Xotabych\*"; DestDir: "{#ResModsConf}\Xotabych\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\08;
Source: "TimeSpent\Xotabych.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\08\1;
Source: "TimeSpent\Xotabych_Armagomen.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\08\2;

Source: "TimeSpent\EZorg\*"; DestDir: "{#ResModsConf}\EZorg\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\09;
Source: "TimeSpent\EZorg.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\09;

Source: "TimeSpent\mcg00\*"; DestDir: "{#ResModsConf}\mcg00\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\10;
Source: "TimeSpent\mcg00.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\10;

Source: "TimeSpent\_bes_1\*"; DestDir: "{#ResModsConf}\_bes_1\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\12;
Source: "TimeSpent\_bes_1.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\12;

Source: "TimeSpent\Chepizhko_Kirill\*"; DestDir: "{#ResModsConf}\Chepizhko_Kirill\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\13;
Source: "TimeSpent\Chepizhko_Kirill.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\13;

Source: "TimeSpent\YasenKrasen\*"; DestDir: "{#ResModsConf}\YasenKrasen\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\14;
Source: "TimeSpent\YK_Vanilla.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\14\v;
Source: "TimeSpent\YK_V1.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\14\1;
Source: "TimeSpent\YK_V2.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\14\2;
Source: "TimeSpent\YK_V3.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\14\3;

Source: "TimeSpent\wlad1164\*"; DestDir: "{#ResModsConf}\wlad1164\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\15;
Source: "TimeSpent\wlad1164.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\15;

Source: "TimeSpent\2rokk\*"; DestDir: "{#ResModsConf}\2rokk\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\16;
Source: "TimeSpent\2rokk.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\16;

Source: "TimeSpent\Magistr\*"; DestDir: "{#ResModsConf}\Magistr\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\18;
Source: "TimeSpent\MagistrF.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\18\s\1;
Source: "TimeSpent\MagistrM.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\18\s\2;
Source: "TimeSpent\Magistr\Magistr_CResults.json"; DestDir: "{#ResModsConf}\TimeSpent\"; Flags: ignoreversion; Components: Stat\18\r\1;
Source: "TimeSpent\Magistr\Magistr_FResults.json"; DestDir: "{#ResModsConf}\TimeSpent\"; Flags: ignoreversion; Components: Stat\18\r\2;

Source: "TimeSpent\ShowIn1.json"; DestDir: "{#ResModsConf}\TimeSpent"; Flags: ignoreversion; Components: Stat\17\1;
Source: "TimeSpent\ShowIn2.json"; DestDir: "{#ResModsConf}\TimeSpent"; Flags: ignoreversion; Components: Stat\17\2;
Source: "TimeSpent\ShowIn3.json"; DestDir: "{#ResModsConf}\TimeSpent"; Flags: ignoreversion; Components: Stat\17\3;

Source: "TimeSpent\vlad_cs_sr\*"; DestDir: "{#ResModsConf}\vlad_cs_sr\"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: Stat\19;
Source: "TimeSpent\vlad_cs_sr.json"; DestDir: "{#ResModsConf}"; DestName: "TimeSpent.json"; Flags: ignoreversion; Components: Stat\19;
