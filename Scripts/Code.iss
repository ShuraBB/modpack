[code]
procedure CreateBackup;
begin
	if IsTaskSelected('Backup') then
	begin
		CopyDir(ExpandConstant('{app}\mods'), ExpandConstant('{app}\Backup\'));
		CopyDir(ExpandConstant('{app}\res_mods'), ExpandConstant('{app}\Backup\'));
	end;
	if IsTaskSelected('DelMods') then
	begin
		DelDir(ExpandConstant('{app}\mods\'), False);
		DelDir(ExpandConstant('{app}\res_mods\'), False);
		ForceDirectories(ExpandConstant('{#ModsDest}'))
		ForceDirectories(ExpandConstant('{#ResModsDest}'))
	end;
	if IsTaskSelected('Cashe') then
	begin
		DelDir(ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks\account_caches'), False);
		DelDir(ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks\battle_results'), False);
		DelDir(ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks\clan_cache'), False);
		DelDir(ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks\custom_data'), False);
		DelDir(ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks\dossier_cache'), False);
		DelDir(ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks\messenger_cache'), False);
		DelDir(ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks\profile'), False);
		DelDir(ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks\veh_cmp_cache'), False);
		DelDir(ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks\storage_cache'), False);
		DelDir(ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks\web_cache'), False);
	end;

end;


type
  TPositionStorage = array of Integer;

var
  CompPageModified: Boolean;
  CompPagePositions: TPositionStorage;

procedure SaveComponentsPage(out Storage: TPositionStorage);
begin
  SetArrayLength(Storage, 20);
	Storage[0] := WizardForm.Height;
	Storage[1] := WizardForm.Width;
	Storage[2] := WizardForm.OuterNotebook.Height;
	Storage[3] := WizardForm.OuterNotebook.Width;
	Storage[4] := WizardForm.InnerNotebook.Height;
	Storage[5] := WizardForm.InnerNotebook.Width;
	Storage[6] := WizardForm.MainPanel.Width;
	Storage[7] := WizardForm.WizardSmallBitmapImage.Left;
	Storage[8] := WizardForm.Bevel1.Width;
	Storage[9] := WizardForm.ComponentsDiskSpaceLabel.Top;
	Storage[10] := WizardForm.Bevel.Top;
	Storage[11] := WizardForm.Bevel.Width;
	Storage[12] := WizardForm.BeveledLabel.Top;
	Storage[13] := WizardForm.NextButton.Top;
	Storage[14] := WizardForm.NextButton.Left;
	Storage[15] := WizardForm.BackButton.Top;
	Storage[16] := WizardForm.BackButton.Left;
	Storage[17] := WizardForm.CancelButton.Top;
	Storage[18] := WizardForm.CancelButton.Left;
	Storage[19] := WizardForm.PageDescriptionLabel.Width;
end;

procedure LoadComponentsPage(const Storage: TPositionStorage; HeightOffset: Integer; WidthOffset: Integer);
begin
	WizardForm.SelectComponentsLabel.Hide;
	WizardForm.Height := Storage[0] + HeightOffset;
	WizardForm.Width := Storage[1] + WidthOffset;
	WizardForm.OuterNotebook.Height := Storage[2] + HeightOffset;
	WizardForm.OuterNotebook.Width := Storage[3] + WidthOffset;
	WizardForm.InnerNotebook.Height := Storage[4] + HeightOffset;
	WizardForm.InnerNotebook.Width := Storage[5] + WidthOffset;
	WizardForm.MainPanel.Width := Storage[6] + WidthOffset;
	WizardForm.PageDescriptionLabel.Width := Storage[19] + WidthOffset;
	WizardForm.WizardSmallBitmapImage.Left := Storage[7] + WidthOffset;
	WizardForm.Bevel1.Width := Storage[8] + WidthOffset;
	WizardForm.TypesCombo.SetBounds(ScaleX(0), ScaleY(0), ScaleX(928), ScaleY(21));
	WizardForm.ComponentsList.SetBounds(ScaleX(0), ScaleY(25), ScaleX(470), ScaleY(430));
	WizardForm.ComponentsDiskSpaceLabel.Top := Storage[9] + HeightOffset;
	WizardForm.Bevel.Top := Storage[10] + HeightOffset;
	WizardForm.Bevel.Width := Storage[11] + WidthOffset;
	WizardForm.BeveledLabel.Top := Storage[12] + HeightOffset;
	WizardForm.NextButton.Top := Storage[13] + HeightOffset;
	WizardForm.NextButton.Left := Storage[14] + WidthOffset;
	WizardForm.BackButton.Top := Storage[15] + HeightOffset;
	WizardForm.BackButton.Left := Storage[16] + WidthOffset;
	WizardForm.CancelButton.Top := Storage[17] + HeightOffset;
	WizardForm.CancelButton.Left := Storage[18] + WidthOffset;
end;

procedure CurPageChanged(CurPageID: Integer);
begin
  if CurpageID = wpSelectComponents then
  begin
    SaveComponentsPage(CompPagePositions);
    LoadComponentsPage(CompPagePositions, ScaleY(252), ScaleX(511));
    CompPageModified := True;
  end
  else
  if CompPageModified then
  begin
    LoadComponentsPage(CompPagePositions, 0, 0);
    CompPageModified := False;
  end;
end;


procedure InitializeWizard;
begin
	ModDescription
	CompPageModified := False;
	WizardForm.ComponentsList.ItemFontStyle[0] := [fsBold];
	WizardForm.ComponentsList.ItemFontStyle[37] := [fsBold];
	WizardForm.ComponentsList.ItemFontStyle[138] := [fsBold];
	WizardForm.ComponentsList.ItemFontStyle[181] := [fsBold];
end;

procedure CurStepChanged(CurStep: TSetupStep);
begin
    if CurStep = ssInstall then CreateBackup
end;


function GetAppFolder(Path: String): String;
begin
	if RegKeyExists(HKCU, 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{1EAC1D02-C6AC-4FA6-9A44-96258C37C812RU}_is1') then
	begin
		RegQueryStringValue(HKCU, 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{1EAC1D02-C6AC-4FA6-9A44-96258C37C812RU}_is1', 'InstallLocation', Path);
		Result := Path
	end else
	if RegKeyExists(HKCU, 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\WOT.RU.PRODUCTION') then
	begin
		RegQueryStringValue(HKCU, 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\WOT.RU.PRODUCTION', 'InstallLocation', Path);
		Result := Path
	end else
	Result := 'C:\Games\World_of_Tanks';
end;