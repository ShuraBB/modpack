[Components]
; *** ������ ������� ***

; ������� ����������
Name: Battle; Description: {cm:BattleMods}; Flags: disablenouninstallwarning Collapsed
Name: Battle\ShockEffect; Description: {cm:ShockEffect}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\FlashEffect; Description: {cm:FlashEffect}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\InertiaCamera; Description: {cm:InertiaCamera}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\RecoilEffect; Description: {cm:RecoilEffect}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\HStabilizer; Description: {cm:HStabilizer}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\Binoculars; Description: {cm:Binoculars}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\HornSound; Description: {cm:HornSound}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\TickingSound; Description: {cm:TickingSound}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\PTips; Description: {cm:PTips}; Types: Default; Flags: disablenouninstallwarning
;Name: Battle\SPanel; Description: {cm:SPanel}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\Timer; Description: {cm:Timer}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\HintPanel; Description: {cm:HintPanel}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\DynSquadD; Description: {cm:DynSquadD}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\MapLog; Description: {cm:MapLog}; Flags: disablenouninstallwarning
Name: Battle\DamLog; Description: {cm:DamLog}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\Speedometer; Description: {cm:Speedometer}; Flags: disablenouninstallwarning Collapsed
Name: Battle\Speedometer\1; Description: {cm:DSpeedometer}; Flags: disablenouninstallwarning exclusive
Name: Battle\Speedometer\2; Description: {cm:Speedometer2All}; Flags: disablenouninstallwarning exclusive
Name: Battle\DCircle; Description: {cm:DCircle}; Flags: disablenouninstallwarning Collapsed
Name: Battle\DCircle\1; Description: {cm:DCircle1}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Battle\DCircle\2; Description: {cm:DCircle2}; Flags: disablenouninstallwarning exclusive
Name: Battle\DCircle\3; Description: {cm:DCircle3}; Flags: disablenouninstallwarning exclusive
Name: Battle\DCircle\4; Description: {cm:DCircle4}; Flags: disablenouninstallwarning exclusive
Name: Battle\SS; Description: {cm:SixthS}; Flags: disablenouninstallwarning Collapsed
Name: Battle\SS\SS10; Description: {cm:s10}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Battle\SS\SS12; Description: {cm:s12}; Flags: disablenouninstallwarning exclusive
Name: Battle\SS\SS6; Description: {cm:s6}; Flags: disablenouninstallwarning exclusive
Name: Battle\SS\SS4; Description: {cm:s4}; Flags: disablenouninstallwarning exclusive
Name: Battle\DInd; Description: {cm:DInd}; Flags: disablenouninstallwarning Collapsed
Name: Battle\DInd\5; Description: {cm:s5}; Flags: disablenouninstallwarning exclusive
Name: Battle\DInd\3; Description: {cm:s3}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Battle\DInd\2; Description: {cm:s2}; Flags: disablenouninstallwarning exclusive
Name: Battle\DInd\1; Description: {cm:s1}; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper; Description: {cm:Sniper}; Flags: disablenouninstallwarning Collapsed
Name: Battle\Sniper\HBrake; Description: {cm:HBrake}; Flags: disablenouninstallwarning Collapsed
Name: Battle\Sniper\HBrake\1; Description: {cm:HBrake1}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\HBrake\2; Description: {cm:HBrake2}; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\ZoomSteps; Description: {cm:ZSteps}; Flags: disablenouninstallwarning Collapsed
Name: Battle\Sniper\ZoomSteps\1; Description: {cm:ZSteps1}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\ZoomSteps\2; Description: {cm:ZSteps2}; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\ZoomSteps\3; Description: {cm:ZSteps3}; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\ZoomSteps\4; Description: {cm:ZSteps4}; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\ZoomSteps\5; Description: {cm:ZSteps5}; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\ZoomStart; Description: {cm:ZStart}; Flags: disablenouninstallwarning dontinheritcheck Collapsed
Name: Battle\Sniper\ZoomStart\0; Description: {cm:ZStart0}; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\ZoomStart\1; Description: {cm:ZStart1}; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\ZoomStart\2; Description: {cm:ZStart2}; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\ZoomStart\3; Description: {cm:ZStart3}; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\ZoomStart\4; Description: {cm:ZStart4}; Flags: disablenouninstallwarning exclusive
Name: Battle\Sniper\ZoomStart\5; Description: {cm:ZStart5}; Flags: disablenouninstallwarning exclusive
Name: Battle\CCamera; Description: {cm:CCamera}; Flags: disablenouninstallwarning Collapsed
Name: Battle\CCamera\1; Description: {cm:CCamera1}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\CCamera\2; Description: {cm:CCamera2}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\CCamera\3; Description: {cm:CCamera3}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\Panel; Description: {cm:Panel}; Flags: disablenouninstallwarning Collapsed
Name: Battle\Panel\L; Description: {cm:PBLoad}; Flags: disablenouninstallwarning Collapsed
Name: Battle\Panel\L\0; Description: {cm:Pr0}; Flags: disablenouninstallwarning exclusive
Name: Battle\Panel\L\1; Description: {cm:Pr1}; Flags: disablenouninstallwarning exclusive
Name: Battle\Panel\L\2; Description: {cm:Pr2}; Flags: disablenouninstallwarning exclusive
Name: Battle\Panel\L\3; Description: {cm:Pr3}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Battle\Panel\L\4; Description: {cm:Pr4}; Flags: disablenouninstallwarning exclusive
Name: Battle\Panel\B; Description: {cm:PBattle}; Flags: disablenouninstallwarning Collapsed
Name: Battle\Panel\B\0; Description: {cm:Pr0}; Flags: disablenouninstallwarning exclusive
Name: Battle\Panel\B\1; Description: {cm:Pr1}; Flags: disablenouninstallwarning exclusive
Name: Battle\Panel\B\2; Description: {cm:Pr2}; Flags: disablenouninstallwarning exclusive
Name: Battle\Panel\B\3; Description: {cm:Pr3}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Battle\Panel\B\4; Description: {cm:Pr4}; Flags: disablenouninstallwarning exclusive
Name: Battle\Panel\noBadge; Description: {cm:noBadge}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\Panel\vip; Description: {cm:vip}; Flags: disablenouninstallwarning dontinheritcheck
Name: Battle\Panel\clan; Description: {cm:clan}; Flags: disablenouninstallwarning dontinheritcheck

; ������� ���������
Name: Battle\MiniMap; Description: {cm:MiniMap}; Flags: disablenouninstallwarning Collapsed
Name: Battle\MiniMap\MapCentr; Description: {cm:MapCentr}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\MiniMap\MapFilter; Description: {cm:MapFilter}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\MiniMap\ClickDead; Description: {cm:ClickDead}; Flags: disablenouninstallwarning
Name: Battle\MiniMap\DelMessages; Description: {cm:DelMessages}; Flags: disablenouninstallwarning
Name: Battle\MiniMap\MapDView; Description: {cm:MapDView}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\MiniMap\MapHAngle; Description: {cm:MapHAngle}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\MiniMap\MapLabel; Description: {cm:MapLabel}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\MiniMap\Destroyed; Description: {cm:ShowDestroyed}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\MiniMap\Duration; Description: {cm:Duration}; Flags: disablenouninstallwarning Collapsed
Name: Battle\MiniMap\Duration\s1; Description: {cm:s1}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\Duration\s2; Description: {cm:s2}; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\Duration\s3; Description: {cm:s3}; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\Duration\s4; Description: {cm:s4}; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\MapMain; Description: {cm:MapMain}; Flags: disablenouninstallwarning Collapsed
Name: Battle\MiniMap\MapMain\MapHD; Description: {cm:MapHD}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\MapMain\MapTact; Description: {cm:MapTact}; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\MapMain\MapHM; Description: {cm:MapHM}; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\MapAlt; Description: {cm:MapAlt}; Flags: disablenouninstallwarning Collapsed
Name: Battle\MiniMap\MapAlt\MapHD; Description: {cm:MapHD}; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\MapAlt\MapTact; Description: {cm:MapTact}; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\MapAlt\MapHM; Description: {cm:MapHM}; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\MapIcons; Description: {cm:MapIcons}; Flags: disablenouninstallwarning Collapsed
Name: Battle\MiniMap\MapIcons\MapHD; Description: {cm:MapHD}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\MapIcons\MapTact; Description: {cm:MapTact}; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\MapCircle; Description: {cm:MapCircle}; Flags: disablenouninstallwarning Collapsed
Name: Battle\MiniMap\MapCircle\MapCircle1; Description: {cm:MapCircle1}; Types: Default; Flags: disablenouninstallwarning exclusive
Name: Battle\MiniMap\MapCircle\MapCircle2; Description: {cm:MapCircle2}; Flags: disablenouninstallwarning exclusive

; ������� ��������������
Name: Battle\ChatFilter; Description: {cm:ChatFilter}; Types: Default; Flags: disablenouninstallwarning
Name: Battle\NotFilterYou; Description: {cm:NotFilterYou}; Flags: disablenouninstallwarning
Name: Battle\ReplayChat; Description: {cm:ReplayChat}; Flags: disablenouninstallwarning
