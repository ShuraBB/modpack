[InstallDelete]
Type: files; Name: "{#ResModsDest}\BBModsSDK.pyc"; Components: not (Hangar Battle Stat);
Type: files; Name: "{#ModsDest}\ShuraBB.MinimalisticSight.wotmod"; Components: not Other\Sight;
Type: filesandordirs; Name: "{app}\mods\configs\crosshairs"; Components: not Other\Sight;
Type: files; Name: "{#GuiDest}\flash\atlases\battleAtlas.*"; Components: not Other\Korolins;
Type: filesandordirs; Name: "{#ResModsConf}\Docs"; Components: not Other\docs;

[Files]
; ������
Source: "Mods\BBModsSDK.pyc"; DestDir: "{#ResModsDest}"; Components: Hangar Battle Stat;

Source: "Other\ShuraBB.MinimalisticSight.wotmod"; DestDir: "{#ModsDest}"; Components: Other\Sight\MSight;
Source: "Other\WhiteSights.wotmod"; DestDir: "{#ModsDest}"; Components: Other\Sight\WSight;
Source: "Other\battleAtlas.dds"; DestDir: "{#GuiDest}\flash\atlases"; Components: Other\Korolins;
Source: "Other\battleAtlas.xml"; DestDir: "{#GuiDest}\flash\atlases"; Components: Other\Korolins;
Source: "Other\sixthSense.mp3"; DestDir: "{#AudioDest}"; Components: Other\sixthSense\1;
Source: "Other\sixthSense2.mp3"; DestDir: "{#AudioDest}"; DestName: "sixthSense.mp3"; Components: Other\sixthSense\2;
Source: "Docs\*"; DestDir: "{#ResModsConf}\Docs"; Components: Other\docs;
Source: "Other\ProfileCleaning.cmd"; DestDir: "{userdesktop}"; Components: Other\PCleaning;
